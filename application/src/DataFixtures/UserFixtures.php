<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class UserFixtures extends Fixture
{
    private $count = 6;
    private $userData;

    public function __construct()
    {
        $this->userData = [
            'user_1' => [
                'userName' => 'user1',
                'userEmail' => 'user1@test.pl',
                'userPhone' => '123456789',
            ],
            'user_2' => [
                'userName' => 'user2',
                'userEmail' => 'user2@test.pl',
                'userPhone' => '321456980',
            ],
            'user_3' => [
                'userName' => 'user3',
                'userEmail' => 'user3@test.pl',
                'userPhone' => '897123455',
            ],
            'user_4' => [
                'userName' => 'user4',
                'userEmail' => 'user4@test.pl',
                'userPhone' => '987345512',
            ],
            'user_5' => [
                'userName' => 'user5',
                'userEmail' => 'user5@test.pl',
                'userPhone' => '987654321',
            ],
            'user_6' => [
                'userName' => 'user6',
                'userEmail' => 'user6@test.pl',
                'userPhone' => '456321987',
            ]
        ];
    }

    public function load(ObjectManager $manager)
    {
        for ($i = 1; $i <= $this->count; $i++) {
            $user = new User();
            $user->setUserName($this->userData['user_'.$i]['userName']);
            $user->setUserEmail($this->userData['user_'.$i]['userEmail']);
            $user->setUserPhone($this->userData['user_'.$i]['userPhone']);

            $manager->persist($user);
            $manager->flush();
        }
    }
}
