clone from https://gitlab.com/Sicinski/userApi.git
cd to application dir
type composer update
you need to set in .env under DATABASE_URL valid dbuserData( ex. DATABASE_URL=mysql://db_user:db_pass@database_host:port/db_name
after composer update
type bin/console doctrine:database:create
then type bin/console doctrine:migrations:migrate
lastly use bin/console doctrine:fixtures:load

open in browser and you should see under / link user list.

sql to create db and table:(seriously not needed doctrine will suffice :) )
CREATE DATABASE `userapi`;
USE `userapi`;
CREATE TABLE users (
    id_user int NULL AUTO_INCREMENT,
    user_name varchar(150),
    user_email varchar(200),
    user_phone varchar(11),
	PRIMARY KEY (id_user)
);
